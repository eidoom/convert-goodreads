{%- macro comma(loop) -%}
{% if not loop.last %}, {% endif %}
{%- endmacro -%}
+++
title = "{{ title }}"
description = ""
date = {{ date_added }}
updated = {{ today }}
draft = false
[taxonomies]
media = ["book"]
tags = ["novel"]
years = [{% for year in years %}"{{ year }}"{{ comma(loop) }}{% endfor %}]
artists = [{% for author in authors %}"{{ author }}"{{ comma(loop) }}{% endfor %}]
series = [{% if series %}"{{ series }}"{% endif %}]
universe = []
[extra]
published = {{ today }}
consumed = [["{{ date_started }}", "{{ date_read }}"]]
{% if number %}number = [{{ number }}]{% else %}#number = {% endif %}
#chrono =
links = []
+++

