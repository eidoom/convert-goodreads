#!/usr/bin/env python3

import datetime
import os
import re
import csv

import jinja2


def frmt_date(date):
    return date.replace("/", "-")


if __name__ == "__main__":
    with open("goodreads_library_export.csv", "r") as f:
        r = csv.DictReader(f)
        d = [row for row in r]

    try:
        os.mkdir("output")
    except FileExistsError:
        pass

    today = datetime.date.today()

    env = jinja2.Environment(loader=jinja2.FileSystemLoader("."))
    template = env.get_template("template.md")

    p = re.compile(r"(.*) \(([^,]*),? #?(.*)\)")
    ap = re.compile(r"(\w\.)(\w\.)")

    res = []

    for e in d:
        if e["Date Read"]:
            subs = {"today": today}

            subs["date_read"] = datetime.date.fromisoformat(frmt_date(e["Date Read"]))

            m = re.match(p, e["Title"])
            if m:
                subs["title"] = m.group(1)
                subs["series"] = m.group(2)
                subs["number"] = m.group(3)
            else:
                subs["title"] = e["Title"]

            subs["years"] = [e["Original Publication Year"]]

            if re.search(ap, e["Author"]):
                e["Author"] = re.sub(ap, r"\1 \2", e["Author"])

            subs["authors"] = [e["Author"]]

            if e["Additional Authors"]:
                subs["authors"].append(e["Additional Authors"])

            subs["date_added"] = datetime.date.fromisoformat(frmt_date(e["Date Added"]))

            subs["slug"] = subs["title"].lower().replace(" ", "-")

            res.append(subs)

    res = sorted(res, key=lambda x: x["date_read"])

    for r in res:
        print(r["date_read"], r["title"])

        if 'prev' in locals():
            r["date_started"] = prev
        prev = r["date_read"]

        output = template.render(r)

        with open("output/" + r["slug"] + ".md", "w") as f:
            f.write(output)
